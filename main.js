let allRead = true;

const notifications = [
  { message: 'Lorem', read: true },
  { message: 'Ipsum', read: true },
  { message: 'Dolor', read: true },
  { message: 'Sit', read: false },
  { message: 'Amet', read: true }
];

// check if every notifications are Read or not
const checkReadStatus = (n) => n.read === true;
allRead = checkReadStatus(notifications);